<?php

declare(strict_types=1);

namespace Drupal\font_iconpicker\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;

/**
 * Provides a font icon picker form element.
 *
 * Usage example:
 * @code
 * $form['icon'] = [
 *   '#type' => 'font_iconpicker',
 *   '#title' => $this->t('Icon'),
 *   '#default_value' => 'my-icon',
 *   '#has_search' => TRUE,
 * ];
 * @endcode
 *
 * @FormElement("font_iconpicker")
 */
class FontIconpicker extends Select {

  /**
   * The HTML data attribute prefix.
   *
   * @var string
   */
  const DATA_ATTRIBUTE_PREFIX = 'data-fonticonpicker-';

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $class = static::class;
    $info = parent::getInfo();

    $info['#process'] = array_merge([[$class, 'processFontIconpicker']], $info['#process']);
    $info['#pre_render'][] = [$class, 'preRenderFontIconpicker'];

    // Add a search bar on widget.
    $info['#has_search'] = FALSE;

    return $info;
  }

  /**
   * Prepares a #type 'font_iconpicker' render element.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #name, #value, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderFontIconpicker(array $element): array {
    $settings = \Drupal::config('font_iconpicker.settings');

    $element['#attributes']['class'][] = 'font-iconpicker-element';

    // Display empty icon when field is optional.
    $element['#attributes'][static::DATA_ATTRIBUTE_PREFIX . 'empty-icon'] = $element['#required'] ? '': 'true';

    // Display field with search bar.
    $element['#attributes'][static::DATA_ATTRIBUTE_PREFIX . 'has-search'] = $element['#has_search'] ? 'true' : '';

    // Attach font icon picker library.
    $element['#attached']['library'][] = 'font_iconpicker/form-element';
    $element['#attached']['drupalSettings']['font_iconpicker'] = [
      'additional_class' => $settings->get('additional_class'),
      'data_attribute_prefix' => static::DATA_ATTRIBUTE_PREFIX,
      'theme' => $settings->get('theme'),
    ];

    return $element;
  }

  /**
   * Processes a #type 'font_iconpicker' render element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @see _form_validate()
   */
  public static function processFontIconpicker(
    array &$element,
    FormStateInterface $form_state,
    array &$complete_form
  ): array {
    // Override options.
    try {

      $iconsAvailable = \Drupal::service('font_iconpicker.icon_helper')
        ->getIconsAvailable();

      $element['#options'] = array_combine($iconsAvailable, $iconsAvailable);
    }
    catch (\LogicException $e) {
      \Drupal::service('logger.factory')->get('font_iconpicker')
        ->error($e->getMessage());

      $element['#options'] = [];
    }

    // Define empty value to empty string instead of "_none".
    $element['#empty_value'] = '';

    // Define default value if field is required.
    if ($element['#required'] && empty($element['#default_value'])) {
      $element['#default_value'] = key($element['#options']);
    }

    return $element;
  }

}
