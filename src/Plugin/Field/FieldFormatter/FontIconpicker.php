<?php

declare(strict_types=1);

namespace Drupal\font_iconpicker\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\font_iconpicker\IconHelperInterface;

/**
 * Plugin implementation of the 'font_iconpicker' formatter.
 *
 * @FieldFormatter(
 *   id = "font_iconpicker",
 *   label = @Translation("Font Icon Picker"),
 *   field_types = {
 *     "font_iconpicker",
 *   }
 * )
 */
class FontIconpicker extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'font_icon',
        '#icon' => Html::escape($item->value),
        '#attached' => [
          'library' => [
            'font_iconpicker/' . IconHelperInterface::FONT_LIBRARY_NAME,
          ],
        ],
      ];
    }

    return $elements;
  }

}
