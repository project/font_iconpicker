<?php

declare(strict_types=1);

namespace Drupal\font_iconpicker;

/**
 * Helper interface.
 *
 * @package Drupal\font_iconpicker
 */
interface IconHelperInterface {

  /**
   * The custom font library name (declared dynamically).
   *
   * @var string
   */
  const FONT_LIBRARY_NAME = 'font-custom';

  /**
   * Get icons available in the custom font.
   *
   * @return string[]
   *   The icons available.
   */
  public function getIconsAvailable(): array;

}
