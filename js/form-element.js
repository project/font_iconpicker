/**
 * @file
 * Font Iconpicker behavior.
 */

(($, Drupal, once) => {
  Drupal.behaviors.fontIconpicker = {
    attach(context, settings) {
      $(once("jsFontIconpicker", "select.font-iconpicker-element", context)).each((index, element) => {
        const $element = $(element);

        // Initialize icon picker.
        $element.fontIconPicker({
          allCategoryText: Drupal.t("From all categories"),
          searchPlaceholder: Drupal.t("Search icons"),
          unCategorizedText: Drupal.t("Uncategorized"),
          theme: `fip-${settings.font_iconpicker.theme.replace("-", "")}`,
          emptyIcon: !!$element.attr(`${settings.font_iconpicker.data_attribute_prefix}empty-icon`),
          hasSearch: !!$element.attr(`${settings.font_iconpicker.data_attribute_prefix}has-search`),
          iconGenerator: function (icon) {
            let iconClass = [icon];

            if (settings.font_iconpicker.additional_class) {
              iconClass.push(settings.font_iconpicker.additional_class);
            }

            return "<span class=\"" + iconClass.join(" ")  + "\" aria-hidden=\"true\"></span>";
          }
        });
      });
    }
  };
})(jQuery, Drupal, once);
