<?php

declare(strict_types=1);

namespace Drupal\font_iconpicker\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'font_iconpicker' widget.
 *
 * @FieldWidget(
 *   id = "font_iconpicker",
 *   label = @Translation("Font Icon Picker"),
 *   field_types = {
 *     "font_iconpicker",
 *   }
 * )
 */
class FontIconpicker extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'has_search' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = [];

    $elements['has_search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display search input'),
      '#default_value' => $this->getSetting('has_search'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];

    $summary[] = $this->t('Display search input: @value', [
      '@value' => $this->getSetting('has_search') ? $this->t('yes') : $this->t('no'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ): array {
    $element['value'] = $element + [
      '#type' => 'font_iconpicker',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#has_search' => (bool) $this->getSetting('has_search'),
    ];

    return $element;
  }

}
